import { FC } from "react"
import { MainLayout } from "../components"
import { IPropsPages } from "../interfaces"



export const Home: FC<IPropsPages> = ({title}) => {

  
  return (
    <MainLayout
      title={title}
    >

    </MainLayout>
  )
}


export default Home